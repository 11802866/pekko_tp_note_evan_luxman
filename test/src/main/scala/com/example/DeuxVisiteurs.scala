package com.example

import org.apache.pekko.actor.typed.ActorRef
import org.apache.pekko.actor.typed.ActorSystem
import org.apache.pekko.actor.typed.Behavior
import org.apache.pekko.actor.typed.scaladsl.Behaviors

object DeuxVisiteurs {
sealed trait Command
  case class Salut(s: String, replyTo: ActorRef[DeuxGardiens.Command]) extends Command
  case class Ciao(s: String, replyTo: ActorRef[DeuxGardiens.Command]) extends Command

  def apply(): Behavior[Command] = Behaviors.receive { (context, message) =>
    message match {
      case Salut(s, replyTo) =>
        println(s)
        replyTo ! DeuxGardiens.Hello("Hello, Gardien!", context.self)
        Behaviors.same
      case Ciao(s, replyTo) =>
        println(s)
        replyTo ! DeuxGardiens.AuRevoir("Aurevoir, Gardien!", context.self)
        Behaviors.same
    }
  }
}

object DeuxGardiens {
  sealed trait Command
  case object Start extends Command
  case class Hello(s: String, replyTo: ActorRef[DeuxVisiteurs.Command]) extends Command
  case class AuRevoir(s: String, replyTo: ActorRef[DeuxVisiteurs.Command]) extends Command

  def apply(): Behavior[Command] = Behaviors.setup { context =>
    var auRevoirCount = 0 // Compteur pour les messages "au revoir"
    Behaviors.receiveMessage {
      case Start =>
        val visiteur1 = context.spawn(DeuxVisiteurs(), "visiteur1")
        val visiteur2 = context.spawn(DeuxVisiteurs(), "visiteur2")
        visiteur1 ! DeuxVisiteurs.Salut("Salut, Visiteur 1!", context.self)
        visiteur2 ! DeuxVisiteurs.Salut("Salut, Visiteur 2!", context.self)
        Behaviors.same
//On a corrigé le Ciao afin que le Visiteur puisse ensuite avoir l'ActorRef afin de repondre au Ciao du Gardien  
      case Hello(s, replyTo) =>
        println(s)
        replyTo ! DeuxVisiteurs.Ciao("Ciao, Visiteur!", context.self)
        Behaviors.same
      case AuRevoir(s, replyTo) =>
        println(s)
        auRevoirCount += 1
        if (auRevoirCount == 2) {
          // S'arrête seulement après avoir reçu deux "au revoir"
          Behaviors.stopped
        } else {
          Behaviors.same
        }
    }
  }
}

/*object Main extends App {
  val system: ActorSystem[DeuxGardiens.Command] = ActorSystem(DeuxGardiens(), "SystemeGardien")
  system ! DeuxGardiens.Start
}*/
