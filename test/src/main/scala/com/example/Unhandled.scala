import org.apache.pekko.actor.typed.ActorRef
import org.apache.pekko.actor.typed.ActorSystem
import org.apache.pekko.actor.typed.Behavior
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import scala.concurrent.duration._
import org.apache.pekko.actor.typed.scaladsl.ActorContext
import org.apache.pekko.actor.typed.SupervisorStrategy


object Gardien {
  sealed trait Command
  case object Start extends Command
  case class Hello(s: String, replyTo: ActorRef[Visiteur.Command]) extends Command
  case class AuRevoir(s: String) extends Command

  def apply(): Behavior[Command] = Behaviors.supervise(Behaviors.setup[Command] { context: ActorContext[Command] =>
    Behaviors.receiveMessage {
      case Start =>
        val visiteur = context.spawn(Visiteur(), "visiteur")
        visiteur ! Visiteur.Salut("Salut, Visiteur!", context.self)
        Behaviors.same
//On a corrigé le Ciao afin que le Visiteur puisse ensuite avoir l'ActorRef afin de repondre au Ciao du Gardien  
      case Hello(s, replyTo) =>
        println(s)
        replyTo ! Visiteur.Ciao("Ciao, Visiteur!")
        Behaviors.same
      case AuRevoir(s) =>
        Behaviors.same
    }
  }).onFailure[Exception](SupervisorStrategy.restart)
}

object Visiteur {
sealed trait Command
  case class Salut(s: String, replyTo: ActorRef[Gardien.Command]) extends Command
  case class Ciao(s: String) extends Command

  def apply(): Behavior[Command] = Behaviors.supervise(Behaviors.receive[Command] { (context: ActorContext[Command], message: Command) =>
    message match {
      /*case Salut(s, replyTo) =>
        println(s)
        replyTo ! Gardien.Hello("Hello, Gardien!", context.self)
       Behaviors.same*/
      case Ciao(s) =>
        println(s)
        Behaviors.stopped
    }
  }).onFailure[Exception](SupervisorStrategy.restart)
}

/*
L'erreur indique que notre code peut échouer si l'entrée est de type Salut(_, _), comme nous n'avons pas géré le cas Salut dans notre bloc match.
En Scala, lorsqu'on utilise match pour effectuer une correspondance sur les messages reçus par un acteur, il faut traiter tous les cas possibles. 
Si un cas est manquant, Scala génère un avertissement (ou une erreur) pour dire que la correspondance n'est pas exhaustive.
Dans notre code, on a commenté le cas Salut dans Visiteur.apply().
 Lorsque l'acteur Visiteur reçoit un message Salut, il ne sait pas comment le traiter, ce qui provoque une scala.MatchError.
*/

object Main extends App {
  val system: ActorSystem[Gardien.Command] = ActorSystem(Gardien(), "SystemeGardien")
  system ! Gardien.Start
}