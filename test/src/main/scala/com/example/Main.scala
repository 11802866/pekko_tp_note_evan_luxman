/*import org.apache.pekko.actor.typed.ActorRef
import org.apache.pekko.actor.typed.ActorSystem
import org.apache.pekko.actor.typed.Behavior
import org.apache.pekko.actor.typed.scaladsl.Behaviors

object Gardien {
  sealed trait Command
  case object Start extends Command
  case class Hello(s: String, replyTo: ActorRef[Visiteur.Command]) extends Command
  case class AuRevoir(s: String) extends Command

  def apply(): Behavior[Command] = Behaviors.setup { context =>
    Behaviors.receiveMessage {
      case Start =>
        val visiteur = context.spawn(Visiteur(), "visiteur")
        visiteur ! Visiteur.Salut("Salut, Visiteur!", context.self)
        Behaviors.same
//On a corrigé le Ciao afin que le Visiteur puisse ensuite avoir l'ActorRef afin de repondre au Ciao du Gardien  
      case Hello(s, replyTo) =>
        println(s)
        replyTo ! Visiteur.Ciao("Ciao, Visiteur!")
        Behaviors.same
      case AuRevoir(s) =>
        Behaviors.same
    }
  }
}

object Visiteur {
sealed trait Command
  case class Salut(s: String, replyTo: ActorRef[Gardien.Command]) extends Command
  case class Ciao(s: String) extends Command

  def apply(): Behavior[Command] = Behaviors.receive { (context, message) =>
    message match {
      case Salut(s, replyTo) =>
        println(s)
        replyTo ! Gardien.Hello("Hello, Gardien!", context.self)
        Behaviors.same
      case Ciao(s) =>
        println(s)
        Behaviors.stopped
    }
  }
}

/*object Main extends App {
  val system: ActorSystem[Gardien.Command] = ActorSystem(Gardien(), "SystemeGardien")
  system ! Gardien.Start
}*/
*/